#include <Wire.h>
#include <Chirp.h> 
#include <SoftwareSerial.h>

Chirp chirp;
Sensors sensor1;
Sensors sensor2;
unsigned short sensors_d[8], sensors_l[8];
unsigned short sensors_d2[8], sensors_l2[8];
unsigned short *temp_d;
int look_auto, look_manual, send_state_hz;
bool incollision;

bool avoiding_enable = true;
bool following_enable = true;
bool requesting_state_enable = false;

const byte RX_PIN = 11;
const byte TX_PIN = 10;
const int BLUETOOTH_BAUD_RATE = 9600;
const int NB_MAX = 16; //max number of bytes (depends on the app)
const byte ENDT_C1 = 191; //end of transmission , first byte (getBytes java charset/based)
const byte ENDT_C2 = 189; //second byte of eot
int eot_flag = 0; //noend, 1 first c, 2 c then endoftransmission

const unsigned int AUTO = 0;
const unsigned int MANUAL = 1;
unsigned int mode = AUTO; //

byte inbuffer[NB_MAX];
int idx_buffer=0;

/*linear and angular velocities for joy control*/
int joy_v = 0;
int joy_w = 0;
const float W_MAX = (100*WHEEL_DIAM)/WHEEL_DIST;
const float V_MAX = 100*(WHEEL_DIAM/2);

SoftwareSerial bluetooth = SoftwareSerial(RX_PIN, TX_PIN);

void setup() {
  chirp.init();
  Wire.begin(); 
  Serial.begin(9600);
  sensor1.begin((uint8_t)0x3, (uint8_t)0x2);
  sensor2.begin((uint8_t)0xF, (uint8_t)0xB);

  pinMode(RX_PIN, INPUT);
  pinMode(TX_PIN, OUTPUT);
  bluetooth.begin(BLUETOOTH_BAUD_RATE);
  /*
  bluetooth.begin(9600);  
  bluetooth.print("$$$");
  delay(100);
  bluetooth.println("U,9600,N");
  bluetooth.begin(9600);
  */
  
  //Serial.println("Init");
  //sendmessage("Wheelly says hi.\n");  

  mode = AUTO;
  look_auto = look_manual = send_state_hz = 0;
  incollision = false;
  avoiding_enable = true;
  following_enable = false;  
  requesting_state_enable = false;

  /*
  readsensordata();
  sendrobotstate();
  */
} 

void cleanbuffer(){
  for (int i=0; i< NB_MAX; i++)
    inbuffer[i] = 0;  
  idx_buffer = 0;
}

void addtobuffer(byte v){
   inbuffer[idx_buffer] = v;
   idx_buffer++;
   if (idx_buffer==NB_MAX)
    idx_buffer--;
}

void logdisplay(String str, bool ln){
  
  if (ln)
      Serial.println (str);
    else
      Serial.print(str);
  
}

void avoiding (unsigned short front_val){
  //logdisplay(String(front_val), true);
  if(front_val > 50) {
    //chirp.setSpeed(100, -100);
    //logdisplay("Beaware of the wall!",true);
    //sendmessage("Wall!\n");
    transmitledscmds('d',255,1,0x3);   
    transmitledscmds('d',255,1,0x2);
    //delay(10);
    //transmitledscmds('d',0,1,0x3);   
    //transmitledscmds('d',0,1,0x2);
    
    //chirp.setSpeed(100, -100);
    //chirp.turnDegrees(45, 100, false);
    incollision = true;
    //delay(100);
  } else {
    transmitledscmds('d',0,1,0x3);   
    transmitledscmds('d',0,1,0x2);
    //chirp.setSpeed(100, 100);
    //logdisplay("no obstacle", true);
    incollision = false;
  }
}


void avoidingmanual (unsigned short front_val){
  if(front_val > 50) {
    //chirp.setSpeed(100, -100);
    //logdisplay("Wall!",true);
    //sendmessage("Wall!");
    transmitledscmds('d',255,1,0x3);   
    transmitledscmds('d',255,1,0x2);
    //delay(10);
    //transmitledscmds('d',0,1,0x3);   
    //transmitledscmds('d',0,1,0x2);
    
    //chirp.setSpeed(-100, -100);
    incollision = true;
    //chirp.turnDegrees(45, 100, true);
    //delay(80);
  } else {
    incollision = false;
    transmitledscmds('d',0,1,0x3);   
    transmitledscmds('d',0,1,0x2);
    //chirp.setSpeed(100, 100);
  }
}

void following (unsigned short sensors_light[8]){
  int min=0, led=0;
  //logdisplay("Light sensors", true);
  for (int i =0; i<8 ; i++){
    //logdisplay(String(sensors_light[i]),false);
    //logdisplay(" ",false);
    if (sensors_light[i] > sensors_light[min])
      min = i;
  }
  if (sensors_light[min] > 600){
    ////Serial.println("Max i:");
    ////Serial.println(min);
    led = min;
    setsingleled(led,1);
    if (led != 0){
       min = (min<=3)?min:-(8-min);
       ////Serial.print("turn: ");
       ////Serial.println(min*45);
       chirp.turnDegrees(min*45, 100, true);
    }
  }
}

void transmitledscmds(byte c, byte v, byte m, uint8_t add){
  Wire.beginTransmission(add);

  Wire.write(c);
  Wire.write(v);
  Wire.write(m);   
  Wire.endTransmission();
  //delay(10);
}

void setsingleled(int n, int v){
  uint8_t add = 0x3;
  if (n > 7 || n < 0)
    return;
  ////Serial.println("lighting");
  v = (v != 1)? 0: 1;
  if (n > 3){
    add = 0x2;
    n = n-4;
  }
  ////Serial.println(add,HEX);
  ////Serial.println(n);
  Wire.beginTransmission(add);
  Wire.write('c');
  Wire.write(n);
  Wire.write(v);   
  Wire.endTransmission();
  delay(10);  
}

void sendmessage(char msg[]){
  bluetooth.write(msg);
}

void readsensordata(){
  sensor2.getSensors(sensors_d2, sensors_l2);
  //delay(50);
  sensor1.getSensors(sensors_d, sensors_l);
}

void sendrobotstate(){
  if (send_state_hz == 0){
    char robot_state[11]; // 0- msg type, 1-8 info, 9-10 eot
    
    //Serial.println("Sending Robot State ");
    
    robot_state[9]=char(ENDT_C1);
    robot_state[10]=char(ENDT_C2);
    // types= 
    // 0 mode state
    // 1 message code
    // 2 distance sensor
    // 3 light sensor
    // mode state
    robot_state[0] = char(0); 
    robot_state[1] = char(mode);
    robot_state[2] = (byte) avoiding_enable;
    robot_state[3] = (byte) following_enable;
    
    if (incollision)
      robot_state[4] = char(1);
    else
      robot_state[4] = char(0);
      
    
    bluetooth.write(robot_state, 11);
  
    //delay(50);
    robot_state[0] = char(2); 
    for (int i=0; i<8 ;i++){
      robot_state[1+i] = char(sensors_d2[i]);
    }
    bluetooth.write(robot_state, 11);
  
    //delay(50);
    
    robot_state[0] = char(3); 
    for (int i=0; i<8 ;i++){
      robot_state[1+i] = char(sensors_d[i]*254/1000);
    }
    bluetooth.write(robot_state, 11);
    
    //delay(50);
    //Serial.println("Sending Done");
    //requesting_state_enable = false;
  }
  send_state_hz = (send_state_hz+1)%5;
}

void robot(){  
  switch(mode){
    case AUTO:
      //sensor2.getSensors(sensors_d2, sensors_l2);

      if (incollision == true)
        chirp.setSpeed(100, -100);
      else
        chirp.setSpeed(100, 100);
      if (look_auto==0){
        readsensordata();
        //sensor1.getSensors(sensors_d, sensors_l);
        if (following_enable)
          following(sensors_d);
        if (avoiding_enable)
          avoiding(sensors_d2[0]);
        //readsensordata();
        if (requesting_state_enable)
          sendrobotstate();
          
      }
      look_auto = (look_auto+1)%3;
      ////Serial.println(look);    
      
    break;
    case MANUAL:
      //calculate the velocities for each wheel from joy_v and joy_w
      int vr=0,vl=0;
      // scale joy_v and joy_w to avoid passing the speedlimits
      float sjv=0, sjw=0;
      sjv = (joy_v * V_MAX)/148;
      sjw = (joy_w * W_MAX)/150;
      sjv = sjv ;//*0.7;
      sjw = sjw ;//*0.5;
      vr = ((2*sjv)+(sjw*WHEEL_DIST))/(WHEEL_DIAM);
      vl = ((2*sjv)-(sjw*WHEEL_DIST))/(WHEEL_DIAM);           
      //logdisplay("vr: "+String(vr), false);
      //logdisplay("  vl: "+String(vl), true);
      //sensor2.getSensors(sensors_d2, sensors_l2);
      if (incollision== true)
        chirp.setSpeed(-100, -100);
      else
        chirp.setSpeed(vr, vl);      
      if (look_manual==0){
        //logdisplay("  manual avoiding", true);
        readsensordata();
        if (avoiding_enable)
          avoidingmanual(sensors_d2[0]);
        if (requesting_state_enable)
          sendrobotstate();
        
      }
      look_manual = (look_manual+1)%3;
    break;
  }
}

void parsecommands(byte cmd[NB_MAX]){
  byte typec = cmd[0];
  switch(typec){
    case 0:
    logdisplay("mode commands ", true);
      if (cmd[1]==0){
        mode = AUTO;
      }
      if (cmd[1]==1){
        mode = MANUAL;
        chirp.setSpeed(0, 0);
        chirp.update();
      }
      if (cmd[1]==2){//flag setting
        if (cmd[2]!=0){//set var, if not ignore
          avoiding_enable=(cmd[2]==1)?true:false;
        }
        if (cmd[3]!=0){//set var, if not ignore
          following_enable=(cmd[3]==1)?true:false;
        }
      }
      if (cmd[1]==3){//request state of the robot
        if (cmd[2] == 1)
          requesting_state_enable = true;
        else
          requesting_state_enable = false;
      }
      logdisplay("new mode: ", false);
      logdisplay(String(mode), true);
    break;
    case 1:
      logdisplay("control commands ", true);
      byte rad = cmd[1];
      int radial = rad;      
      ////Serial.println(radial,BIN);
      ////Serial.println(cmd[1],BIN);
      //they are degrees, need to be converted
      int angle = word((char)cmd[2],(char)cmd[3]);
      float anglerad = angle * PI/180;
      logdisplay("radial: "+String(radial), false);
      logdisplay("  angle: "+String(angle), true);
      joy_v = radial * sin(anglerad);
      joy_w = radial * cos(anglerad);
      logdisplay("joy_v: "+String(joy_v), false);
      logdisplay("  joy_w: "+String(joy_w), true);
            
    break;
    
  }
}

void checkcommands(){
  int nb;
  //byte cmd[NB_MAX]={0,0,0,0,0,0,0,0};
  byte v = 0;
  
  nb = bluetooth.available();  
  //logdisplay("",true);
  //logdisplay("nb= ",false);
  if (nb>0){
    logdisplay("nb: "+String(nb),true);
    logdisplay("Receiving commands:  ",true);
    for (int i=0; i<nb; i++){
      v = bluetooth.read();
      addtobuffer(v);
      logdisplay(" "+String(v), false);
      if (v == ENDT_C1){
        eot_flag = 1;
      }else
      if (v == ENDT_C2 && eot_flag == 1){
        eot_flag = 2;
        logdisplay(" EOD ", true);      
        parsecommands(inbuffer);
        cleanbuffer();
        eot_flag = 0;
      }
      else
        eot_flag = 0;
      //else{
      //  addtobuffer(v);
      //}
    }
    //logdisplay(String(cmd[1]), true);
    //logdisplay(" ", true);
    //parsecommands(cmd);
  }
}

void loop() {  
  //Serial.println("loop");
  ////Serial.println(chirp.getDistanceSensor(FRONT));
  ////Serial.println(chirp.getLightSensor(FRONT));
  //sensor.getDistanceSensors(sensors_d);  
  //temp_d = chirp.getDistanceSensorData();
  
  checkcommands();
  robot();
  
  //chirp.setSpeed(100, 100);
  //sensor2.getSensors(sensors_d2, sensors_l2);
  //avoiding(sensors_d2[0]);
  
  chirp.update();
  
}
