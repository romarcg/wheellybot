#ifndef Vector2D_guard

#include <Arduino.h>

#define Vector2D_guard

class Vector2D
{
	public:
		Vector2D(float x = 0, float y = 0);
		float length() const;
		float lengthSquared() const;
		float normalize();
		float cross(const Vector2D& v2) const;
		float dot(const Vector2D& v2) const;
	public:
		float x, y;

};
#endif