#include "Vector2D.h"

Vector2D::Vector2D(float x, float y)
{
	this->x = x;
	this->y = y;
}

float Vector2D::cross(const Vector2D& v2) const
{
	return x * v2.y - y * v2.y;
}

float Vector2D::dot(const Vector2D& v2) const
{
	return x * v2.x + y * v2.y;
}

float Vector2D::lengthSquared() const 
{
	return dot(*this);
}

float Vector2D::length() const
{
	return sqrt(lengthSquared());
}

float Vector2D::normalize()
{
	float mag = length();

	if (mag == 0.0) 
		return mag;

	x /= mag;
	y /= mag;

	return mag;
}