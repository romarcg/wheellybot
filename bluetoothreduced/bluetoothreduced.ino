#include <SoftwareSerial.h>

 /*Crossover pins, not as in the extended example bluetoothcm*/
const int RX_PIN = 10;
const int TX_PIN = 11;
const int BLUETOOTH_BAUD_RATE = 9600;
 
SoftwareSerial bluetooth(RX_PIN, TX_PIN);
 
void setup() {
   Serial.begin(9600);
   bluetooth.begin(BLUETOOTH_BAUD_RATE);
}
 
void loop() {
  if (bluetooth.available()) {
    Serial.write(bluetooth.read());
  }
  if (Serial.available()) {
    bluetooth.write(Serial.read());
  }
}
